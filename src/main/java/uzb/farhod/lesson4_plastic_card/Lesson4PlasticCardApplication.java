package uzb.farhod.lesson4_plastic_card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson4PlasticCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson4PlasticCardApplication.class, args);
    }

}
