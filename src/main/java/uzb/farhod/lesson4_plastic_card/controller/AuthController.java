package uzb.farhod.lesson4_plastic_card.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson4_plastic_card.entity.MyUser;
import uzb.farhod.lesson4_plastic_card.payload.LoginDto;
import uzb.farhod.lesson4_plastic_card.repository.UserRepository;
import uzb.farhod.lesson4_plastic_card.security.JwtProvider;
import uzb.farhod.lesson4_plastic_card.service.MyAuthService;

import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    MyAuthService myAuthService;

    @Autowired
    UserRepository userRepository;


    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto){
        return myAuthService.login(loginDto);
    }

    @PostMapping("/register")
    public boolean register(@RequestBody LoginDto loginDto){
       return  myAuthService.register(loginDto);
    }

    @GetMapping("/user/{id}")
    public MyUser getUserById(@PathVariable Integer id){
        Optional<MyUser> byId = userRepository.findById(id);
        return byId.orElse(null);

    }

}
