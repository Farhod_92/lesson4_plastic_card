package uzb.farhod.lesson4_plastic_card.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson4_plastic_card.payload.HistoryDto;
import uzb.farhod.lesson4_plastic_card.payload.SendMoneyDto;
import uzb.farhod.lesson4_plastic_card.service.TransactionService;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
    @Autowired
    TransactionService transactionService;

    @PostMapping
    public HttpEntity<?> sendMoney(@RequestBody SendMoneyDto sendMoneyDto){
      return transactionService.sendMoney(sendMoneyDto);
    }

    @GetMapping
    public ResponseEntity<?> getTransactionHistory(@RequestBody HistoryDto historyDto){
        return transactionService.seeHistory(historyDto);
    }
}
