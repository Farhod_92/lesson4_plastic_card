package uzb.farhod.lesson4_plastic_card.payload;

import lombok.Getter;

@Getter
public class HistoryDto {
    //true bo'lsa income false bo'lsa outcome
    private boolean income;

    private Integer cardId;
}
