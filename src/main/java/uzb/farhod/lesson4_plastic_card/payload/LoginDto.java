package uzb.farhod.lesson4_plastic_card.payload;

import lombok.Data;

@Data
public class LoginDto {

    private String username;

    private String password;
}
