package uzb.farhod.lesson4_plastic_card.payload;

import lombok.Getter;

@Getter
public class SendMoneyDto {
    private Integer senderCardId;
    private Integer sreceiverCardId;
    private Float amount;
    private Integer commissionPercent;
}
