package uzb.farhod.lesson4_plastic_card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson4_plastic_card.entity.Card;

@RepositoryRestResource(path = "card", collectionResourceRel = "list")
public interface CardRepository extends JpaRepository<Card,Integer> {
}
