package uzb.farhod.lesson4_plastic_card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson4_plastic_card.entity.Income;
import uzb.farhod.lesson4_plastic_card.entity.Outcome;

public interface IncomeRepository extends JpaRepository<Income, Integer> {
}
