package uzb.farhod.lesson4_plastic_card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson4_plastic_card.entity.Outcome;

import java.util.List;

public interface OutcomeRepository extends JpaRepository<Outcome, Integer> {
    List<Outcome> findAllByFromCardId(Integer id);
    List<Outcome> findAllByToCardId(Integer id);
}
