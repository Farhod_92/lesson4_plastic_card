package uzb.farhod.lesson4_plastic_card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson4_plastic_card.entity.MyUser;

import java.util.Optional;

public interface UserRepository extends JpaRepository<MyUser, Integer> {
    Optional<MyUser> findByName(String name);
}
