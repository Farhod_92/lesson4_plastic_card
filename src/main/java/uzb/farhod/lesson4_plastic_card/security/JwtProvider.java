package uzb.farhod.lesson4_plastic_card.security;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;

import java.util.Date;

@Component
public class JwtProvider {

    static String secret="juda uzun maxfiy so'z";
    static long expireTime=1000*60*60*24; //1 kun

    public String generateToken(String username){
        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();

        return "Bearer "+token;
    }

    public boolean validateToken(String token){
            try{
                Jwts.parser()
                        .setSigningKey(secret)
                        .parseClaimsJws(token);
                return true;
            }catch (Exception e){
                return false;
            }
    }

    public String getUserNameFromToken(String token){
        String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return username;
    }

}
