package uzb.farhod.lesson4_plastic_card.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson4_plastic_card.entity.MyUser;
import uzb.farhod.lesson4_plastic_card.payload.LoginDto;
import uzb.farhod.lesson4_plastic_card.repository.UserRepository;
import uzb.farhod.lesson4_plastic_card.security.JwtProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MyAuthService implements UserDetailsService {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        List<MyUser> userList=userRepository.findAll();

        for (MyUser myUser : userList) {
            if(myUser.getName().equals(username)){
                User user=new User(myUser.getName(), myUser.getPassword(), new ArrayList<>());
                return user;
            }
        }


        throw new UsernameNotFoundException("bu usernameli user topilmadi");
    }

    public ResponseEntity<?> login(LoginDto loginDto){
        try{
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
            authenticationManager.authenticate(usernamePasswordAuthenticationToken);

            String token = jwtProvider.generateToken(loginDto.getUsername());
            return ResponseEntity.ok(token);
        }catch (Exception e){
            return ResponseEntity.status(401).body("login yoki parol xato");
        }
    }

    public boolean register(LoginDto loginDto) {
        try{
            MyUser newUser = new MyUser();
            newUser.setName(loginDto.getUsername());
            newUser.setPassword(passwordEncoder.encode(loginDto.getPassword()));
            userRepository.save(newUser);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
