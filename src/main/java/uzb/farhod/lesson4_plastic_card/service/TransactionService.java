package uzb.farhod.lesson4_plastic_card.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson4_plastic_card.entity.Card;
import uzb.farhod.lesson4_plastic_card.entity.Income;
import uzb.farhod.lesson4_plastic_card.entity.MyUser;
import uzb.farhod.lesson4_plastic_card.entity.Outcome;
import uzb.farhod.lesson4_plastic_card.payload.HistoryDto;
import uzb.farhod.lesson4_plastic_card.payload.SendMoneyDto;
import uzb.farhod.lesson4_plastic_card.repository.CardRepository;
import uzb.farhod.lesson4_plastic_card.repository.IncomeRepository;
import uzb.farhod.lesson4_plastic_card.repository.OutcomeRepository;
import uzb.farhod.lesson4_plastic_card.repository.UserRepository;
import uzb.farhod.lesson4_plastic_card.security.JwtProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    OutcomeRepository outcomeRepository;

    @Autowired
    IncomeRepository incomeRepository;

    public ResponseEntity<?> sendMoney(SendMoneyDto sendMoneyDto){
        Integer loggedUserId=null;
        float amount=sendMoneyDto.getAmount();
        Integer commissionPercent=sendMoneyDto.getCommissionPercent();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            String userDetails=((UserDetails) principal).getUsername();
            Optional<MyUser> optionalMyUser = userRepository.findByName(userDetails);
            if(optionalMyUser.isPresent())
                loggedUserId=optionalMyUser.get().getId();
        }


        Optional<Card> optionalSenderCard=cardRepository.findById(sendMoneyDto.getSenderCardId());
        if(!optionalSenderCard.isPresent())
            return ResponseEntity.status(409).body("Uzatuvchi kartasi topilmadi");

        Card senderCard= optionalSenderCard.get();

        //Tizimga kirgan user o'z kartasidan pul jo'natayotganini tekshirish
        if(!loggedUserId.equals(senderCard.getUser().getId()))
            return ResponseEntity.status(409).body("karta sizniki emas!");

        if(senderCard.getBalance()< (amount+amount*commissionPercent/100))
            return ResponseEntity.status(409).body("kartangizda mablag'yetarli emas!");

        Optional<Card> optionalReceiverCard=cardRepository.findById(sendMoneyDto.getSreceiverCardId());
        if(!optionalReceiverCard.isPresent())
            return ResponseEntity.status(409).body("Qabul qiluvchi kartasi topilmadi");

        Card receiverCard= optionalReceiverCard.get();

        //Pul o'tkazishni amalga oshirish
        senderCard.setBalance(senderCard.getBalance()-(amount+amount*commissionPercent/100));
        cardRepository.save(senderCard);
        receiverCard.setBalance(receiverCard.getBalance()+amount);
        cardRepository.save(receiverCard);

        Date transactionTime=new Date();

        Outcome outcome=new Outcome();
        outcome.setFromCard(senderCard);
        outcome.setToCard(receiverCard);
        outcome.setAmount(amount);
        outcome.setComissionAmount(amount*commissionPercent/100);
        outcome.setDate(transactionTime);
        outcomeRepository.save(outcome);

        Income income=new Income();
        income.setFromCard(senderCard);
        income.setToCard(receiverCard);
        income.setAmount(amount);
        income.setDate(transactionTime);
        incomeRepository.save(income);

        return ResponseEntity.ok("Tranzaksiya muvaffaqiyatli ");
    }

    public ResponseEntity<?> seeHistory(HistoryDto historyDto){

        Integer loggedUserId=null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            String userDetails=((UserDetails) principal).getUsername();
            Optional<MyUser> optionalMyUser = userRepository.findByName(userDetails);
            if(optionalMyUser.isPresent())
                loggedUserId=optionalMyUser.get().getId();
        }

        Optional<Card> optionalCard=cardRepository.findById(historyDto.getCardId());
        if(!optionalCard.isPresent())
            return ResponseEntity.status(409).body("kartas topilmadi");

        Card card= optionalCard.get();

        //Tizimga kirgan user o'z kartasidan pul jo'natayotganini tekshirish
        if(!loggedUserId.equals(card.getUser().getId()))
            return ResponseEntity.status(409).body("karta sizniki emas!");

        List<Outcome> outcomeList=new ArrayList<>();

        //incomelarni olish
        if (historyDto.isIncome())
           outcomeList= outcomeRepository.findAllByToCardId(historyDto.getCardId());
        //outcomlarni olish
        else
            outcomeList=outcomeRepository.findAllByFromCardId(historyDto.getCardId());

        return ResponseEntity.ok(outcomeList);

    }
}
